<?php

namespace App\Http\Controllers;

use App\Models\Resources;
use App\Client;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;

class ResourcesController extends Controller
{
   
    public function add_resources(Request $request, $cid) {
        $status = "error";
        $message = "Error while add data";
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], $code);
            }
        }

        $resourcesadd = Resources::create([
            'name' => $request->post('name'),
            'status' => $request->post('status'),
            'description' => $request->post('description'),
        ]);

        if($resourcesadd) {
            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }


    
    public function show($cid, $id)
    {
        $status = "true";
        $message = "succes";
        $data = Resources::findOrFail($id);
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], $code);
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }

    public function showall(Resources $resources)
    {    
        $status = "true";
        $message = "succes";
        $data = Resources::all();
        $code = 200;

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], $code);
    }


    public function edit_resources(Request $request, $cid, $id) {
        $status = "error";
        $message = "Error while edit data";
        $data = NULL;
        $code = 500;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        $resources = Resources::find($id);
        $resources->name = $request->input('name');
        $resources->status = $request->input('status');
        $resources->description = $request->input('description');
        $resources->save();

        if($resources) {
            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }


    public function delete($cid, $id) {
        $status = false;
        $message = "Error while delete data Fee";
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        $resources = Resources::find($id);
        $resources->delete();

        if($resources) {
            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message
        ], $code);
    }


    
    public function update(Request $request, Resources $resources)
    {
        //
    }

    
    public function destroy(Resources $resources)
    {
        //
    }
}
