<?php

namespace App\Http\Controllers;

use App\Models\Detail_setting_resources;
use App\Models\Setting_resources;
use App\Models\Resources;
use Illuminate\Http\Request;
use App\Client;

class Detail_setting_resourcesController extends Controller
{
    
    public function show($cid, $id)
    {
        $status = "true";
        $message = "succes";
        $data = Setting_resources::where('id', $id)->get(['id', 'logo', 'name', 'description', 'resources_id']);
        foreach($data as $value){
            $value->resources = Resources::where('id', $value->resources_id)
            ->get(['id', 'description']);
        }
        foreach($data as $detail){
                $detail->fees = Detail_setting_resources::findOrFail($id)
                ->get(['id','type_fee','value_fee', 'minimum_fee']);
        
        }
        
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], $code);
            }
        }
       
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data 
            
        ], $code);
    }

    public function add_setting($cid) {
        $status = "error";
        $message = "Error while add data";
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], $code);
            }
        }

        $set_resourcesadd = Setting_resources::create([
            'logo' => request('logo'),
            'name' => request('name'),
            'url_id' => request('url_id'),
            'description' => request('description'),
            'resources_id' => request('resources_id'),
        ]);

        if($set_resourcesadd) {
            foreach(request('fees') as $detail){
                Detail_setting_resources::create([
                        "fee_id"=>$detail['id'],
                        "type_fee"=>$detail['type_fee'],
                        "value_fee"=>$detail['value_fee'] ,
                        "minimum_fee"=>$detail['minimum_fee'],
                        "setting_resources_id"=>$set_resourcesadd->id,
                ]);   
            }

            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }


    public function showall(Detail_setting_resources $detail_setting_resources)
    {
        //
    }

    
    public function edit(Detail_setting_resources $detail_setting_resources)
    {
        //
    }

    
    public function update(Request $request, Detail_setting_resources $detail_setting_resources)
    {
        //
    }

  
    public function destroy(Detail_setting_resources $detail_setting_resources)
    {
        //
    }
}
