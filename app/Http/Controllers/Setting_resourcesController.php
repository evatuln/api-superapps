<?php

namespace App\Http\Controllers;

use App\Models\Setting_resources;
use App\Models\Detail_setting_resources;
use App\Models\Resources;
use App\Client;
use Illuminate\Http\Request;

class Setting_resourcesController extends Controller
{
    
    public function index()
    {
        //
    }


    public function show($cid, $id)
    {
        $status = "true";
        $message = "succes";
        $data = Setting_resources::where('id', $id)->get(['id', 'logo', 'name', 'description', 'resources_id']);
        foreach($data as $value){
            $value->resources = Resources::where('id', $value->resources_id)
            ->get(['id', 'description']);
        }
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], $code);
            }
        }
       
        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data 
            
        ], $code);
    }

    public function add_setting($cid) {
        $status = "error";
        $message = "Error while add data";
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], $code);
            }
        }

        $set_resourcesadd = Setting_resources::create([
            'logo' => request('logo'),
            'name' => request('name'),
            'url_id' => request('url_id'),
            'description' => request('description'),
            'resources_id' => request('resources_id'),
        ]);

        if($set_resourcesadd) {
            foreach(request('fees') as $detail){
                Detail_setting_resources::create([
                        "fee_id"=>$detail['id'],
                        "type_fee"=>$detail['type_fee'],
                        "value_fee"=>$detail['value_fee'] ,
                        "minimum_fee"=>$detail['minimum_fee'],
                        "setting_resources_id"=>$set_resourcesadd->id,
                ]);   
            }

            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }


    public function edit_setting(Request $request, $cid, $id) {
        $status = "error";
        $message = "Error while edit data";
        $code = 500;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        // $set_resources = Setting_resources::where('id', $id)->put([
        //     'logo' => $request->input('logo'),
        //     'name' => $request->input('name'),
        //     'url_id' => $request->input('url_id'),
        //     'description' => $request->input('description'),
        //     'resources_id' => $request->input('resources_id'),
        // ]);
        // fill(request()->all())

        $set_resources = Setting_resources::find($id);
        $set_resources->logo = $request->input('logo');
        $set_resources->name = $request->input('name');
        $set_resources->url_id = $request->input('url_id');
        $set_resources->description = $request->input('description');
        $set_resources->resources_id = $request->input('resources_id');
        $set_resources->save();

        if($set_resources) {
            foreach(request('fees') as $detail){
                Detail_setting_resources::where('id', $id)->put([
                        "fee_id"=>$detail['id'],
                        "type_fee"=>$detail['type_fee'],
                        "value_fee"=>$detail['value_fee'] ,
                        "minimum_fee"=>$detail['minimum_fee'],
                        "setting_resources_id"=>$set_resources->id,
                ]);   
            }

            $status = true;
            $message = "Success";
            $code = 200;
        }

    
        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }

    
    public function delete($cid, $id) {
        $status = false;
        $message = "Error while delete data Fee";
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        $set_resources = Setting_resources::find($id);
        $set_resources->delete();

        if($set_resources) {
            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message
        ], $code);
    }



    public function edit(Setting_resources $setting_resources)
    {
        //
    }

    
    public function update(Request $request, Setting_resources $setting_resources)
    {
        //
    }

   
    public function destroy(Setting_resources $setting_resources)
    {
        //
    }
}
