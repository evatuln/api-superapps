<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting_resources extends Model
{
    protected $table = "setting_resources";
    public $incrementing = true;
    public $timestamps = true;
    protected $guarded = [];

}
