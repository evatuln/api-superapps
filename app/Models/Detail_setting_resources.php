<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Detail_setting_resources extends Model
{
    protected $table = "detail_setting_resources";
    public $incrementing = true;
    public $timestamps = true;
    protected $guarded = [];

}
