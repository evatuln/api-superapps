<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    protected $table = "fee";
    public $incrementing = true;
    public $timestamps = true;
    protected $guarded = [];
}
