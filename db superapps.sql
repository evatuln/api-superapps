/*
 Navicat Premium Data Transfer

 Source Server         : database_fh
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : superapps

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 19/10/2021 16:13:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_url` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `subdomain` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_address` longtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `client_country` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_state` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_city` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_latitude` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `client_longitude` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `contact_phone` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `created_at` datetime(0) NULL DEFAULT NULL,
  `updated_at` datetime(0) NULL DEFAULT NULL,
  `deleted_at` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES (1, 'mjVmBSJ31Hx2Ag9GCOUS', 28, NULL, 'TAG', NULL, '102', NULL, NULL, NULL, NULL, 'TAG Support', '8123303168', '2021-03-17 06:36:49', '2021-03-25 02:28:04', NULL);
INSERT INTO `client` VALUES (4, 'DdB9XtbsMUKydROKDf6M', 197, 'fh', 'Friederich Herman', '', '102', NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-14 07:01:17', '2021-04-14 07:01:17', NULL);

-- ----------------------------
-- Table structure for detail_setting_resources
-- ----------------------------
DROP TABLE IF EXISTS `detail_setting_resources`;
CREATE TABLE `detail_setting_resources`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `setting_resources_id` bigint(20) UNSIGNED NOT NULL,
  `fee_id` bigint(20) UNSIGNED NOT NULL,
  `type_fee` enum('%','value') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `value_fee` bigint(20) NOT NULL,
  `minimum_fee` bigint(20) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `detail_setting_resources_setting_resources_id_foreign`(`setting_resources_id`) USING BTREE,
  INDEX `detail_setting_resources_fee_id_foreign`(`fee_id`) USING BTREE,
  CONSTRAINT `detail_setting_resources_fee_id_foreign` FOREIGN KEY (`fee_id`) REFERENCES `fee` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `detail_setting_resources_setting_resources_id_foreign` FOREIGN KEY (`setting_resources_id`) REFERENCES `setting_resources` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of detail_setting_resources
-- ----------------------------
INSERT INTO `detail_setting_resources` VALUES (1, 1, 1, '%', 10, 10000, NULL, NULL);
INSERT INTO `detail_setting_resources` VALUES (2, 5, 1, '%', 10, 10000, '2021-10-19 06:59:02', '2021-10-19 06:59:02');
INSERT INTO `detail_setting_resources` VALUES (3, 6, 1, '%', 10, 10000, '2021-10-19 07:04:22', '2021-10-19 07:04:22');
INSERT INTO `detail_setting_resources` VALUES (4, 7, 1, '%', 10, 10000, '2021-10-19 08:45:10', '2021-10-19 08:45:10');
INSERT INTO `detail_setting_resources` VALUES (5, 17, 1, '%', 10, 10000, '2021-10-19 08:53:20', '2021-10-19 08:53:20');
INSERT INTO `detail_setting_resources` VALUES (6, 18, 1, '%', 10, 10000, '2021-10-19 08:53:53', '2021-10-19 08:53:53');
INSERT INTO `detail_setting_resources` VALUES (7, 19, 1, '%', 10, 10000, '2021-10-19 08:54:18', '2021-10-19 08:54:18');

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp(0) NOT NULL DEFAULT current_timestamp(0),
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for fee
-- ----------------------------
DROP TABLE IF EXISTS `fee`;
CREATE TABLE `fee`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fee
-- ----------------------------
INSERT INTO `fee` VALUES (1, 'edit', 'edit lagi', NULL, '2021-10-14 02:34:51');
INSERT INTO `fee` VALUES (2, 'nisacoba', 'pembahasan pembahasan pembahasan pembahasan', '2021-10-13 08:14:50', '2021-10-13 08:14:50');
INSERT INTO `fee` VALUES (3, 'nisacoba', 'pembahasan pembahasan pembahasan pembahasan', '2021-10-13 08:22:23', '2021-10-13 08:22:23');
INSERT INTO `fee` VALUES (4, 'nisacoba', 'pembahasan pembahasan pembahasan pembahasan', '2021-10-13 08:23:20', '2021-10-13 08:23:20');
INSERT INTO `fee` VALUES (5, 'nisacoba', 'pembahasan pembahasan pembahasan pembahasan', '2021-10-13 08:23:59', '2021-10-13 08:23:59');
INSERT INTO `fee` VALUES (9, 'nisacobaqw', 'pembahasan pembahasan pembahasan pembahasan', '2021-10-13 09:22:21', '2021-10-13 09:22:21');
INSERT INTO `fee` VALUES (10, 'nisacobaqw', 'pembahasan pembahasan pembahasan pembahasan', '2021-10-13 09:22:53', '2021-10-13 09:22:53');
INSERT INTO `fee` VALUES (11, 'nisacobaqw', 'pembahasan pembahasan pembahasan pembahasan', '2021-10-13 09:23:09', '2021-10-13 09:23:09');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2021_10_12_032003_create_resources_table', 1);
INSERT INTO `migrations` VALUES (6, '2021_10_12_032548_create_fee_table', 1);
INSERT INTO `migrations` VALUES (7, '2021_10_12_053625_create_resources_table', 2);
INSERT INTO `migrations` VALUES (8, '2021_10_12_053819_create_fee_table', 2);
INSERT INTO `migrations` VALUES (9, '2021_10_12_070340_create_fee_table', 3);
INSERT INTO `migrations` VALUES (10, '2021_10_19_012543_create_setting_resources_table', 4);
INSERT INTO `migrations` VALUES (11, '2021_10_19_020231_create_detail_setting_resources_table', 5);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp(0) NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for resources
-- ----------------------------
DROP TABLE IF EXISTS `resources`;
CREATE TABLE `resources`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('on','off') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of resources
-- ----------------------------
INSERT INTO `resources` VALUES (1, 'edit', 'off', 'edit', NULL, '2021-10-14 02:38:08');
INSERT INTO `resources` VALUES (4, 'nisacoba', 'on', 'pembahasan', '2021-10-19 01:49:26', '2021-10-19 01:49:26');

-- ----------------------------
-- Table structure for setting_resources
-- ----------------------------
DROP TABLE IF EXISTS `setting_resources`;
CREATE TABLE `setting_resources`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `resources_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `setting_resources_resources_id_foreign`(`resources_id`) USING BTREE,
  CONSTRAINT `setting_resources_resources_id_foreign` FOREIGN KEY (`resources_id`) REFERENCES `resources` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of setting_resources
-- ----------------------------
INSERT INTO `setting_resources` VALUES (1, 1, 'iya', '123.gng', NULL, 'asda', NULL, '2021-10-19 08:45:18');
INSERT INTO `setting_resources` VALUES (4, 1, 'iya', 'guys.gng', NULL, 'coba', '2021-10-19 02:39:43', '2021-10-19 09:00:57');
INSERT INTO `setting_resources` VALUES (5, 1, 'iya', '123.gpng', NULL, 'asda', '2021-10-19 06:59:02', '2021-10-19 06:59:02');
INSERT INTO `setting_resources` VALUES (6, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 07:04:22', '2021-10-19 07:04:22');
INSERT INTO `setting_resources` VALUES (7, 1, 'iya', '123.gpng', NULL, 'asda', '2021-10-19 08:45:09', '2021-10-19 08:45:09');
INSERT INTO `setting_resources` VALUES (8, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 08:48:34', '2021-10-19 08:48:34');
INSERT INTO `setting_resources` VALUES (9, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 08:49:52', '2021-10-19 08:49:52');
INSERT INTO `setting_resources` VALUES (10, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 08:49:59', '2021-10-19 08:49:59');
INSERT INTO `setting_resources` VALUES (11, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 08:50:01', '2021-10-19 08:50:01');
INSERT INTO `setting_resources` VALUES (12, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 08:50:03', '2021-10-19 08:50:03');
INSERT INTO `setting_resources` VALUES (13, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 08:50:05', '2021-10-19 08:50:05');
INSERT INTO `setting_resources` VALUES (14, 1, 'iya', '123.gng', NULL, 'asda', '2021-10-19 08:50:08', '2021-10-19 08:50:08');
INSERT INTO `setting_resources` VALUES (15, 1, 'iya', 'edit.gng', NULL, 'asda', '2021-10-19 08:50:44', '2021-10-19 08:50:44');
INSERT INTO `setting_resources` VALUES (16, 1, 'iya', 'edit.gng', NULL, 'asda', '2021-10-19 08:52:52', '2021-10-19 08:52:52');
INSERT INTO `setting_resources` VALUES (17, 1, 'iya', 'edit.gng', NULL, 'asda', '2021-10-19 08:53:20', '2021-10-19 08:53:20');
INSERT INTO `setting_resources` VALUES (18, 1, 'iya', 'edit.gng', NULL, 'asda', '2021-10-19 08:53:53', '2021-10-19 08:53:53');
INSERT INTO `setting_resources` VALUES (19, 1, 'iya', 'edit.gng', NULL, 'coba', '2021-10-19 08:54:18', '2021-10-19 08:54:18');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp(0) NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------

SET FOREIGN_KEY_CHECKS = 1;
