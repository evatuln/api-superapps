<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_resources', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('resources_id');
            $table->foreign('resources_id')->references('id')->on('resources');
            $table->string('name');
            $table->string('logo');
            $table->string('url_id');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_resources');
    }
}
