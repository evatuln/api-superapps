<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailSettingResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_setting_resources', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('setting_resources_id');
            $table->foreign('setting_resources_id')->references('id')->on('setting_resources');
            $table->unsignedBigInteger('fee_id');
            $table->foreign('fee_id')->references('id')->on('fee');
            $table->enum('type_fee', ['%', 'value']);
            $table->bigInteger('value_fee');
            $table->bigInteger('minimum_fee');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_setting_resources');
    }
}
